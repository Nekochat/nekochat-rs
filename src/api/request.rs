use serde::{Deserialize, Serialize};

use super::{ChannelId, MessageId, UserId, RoomId, SessionToken};

#[derive(Debug, Serialize, Deserialize)]
pub struct Request {
    pub id: u32,
    #[serde(flatten)]
    pub kind: RequestKind,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum RequestKind {
    #[serde(rename = "user.create")]
    UserCreate {
        name: String,
        password: String,
    },
    #[serde(rename = "user.get")]
    UserGet {
        #[serde(rename = "userId")]
        user_id: UserId,
    },
    #[serde(rename = "user.get.self")]
    UserGetSelf,
    #[serde(rename = "user.get.self.room")]
    UserGetSelfRoom,
    #[serde(rename = "session.create")]
    SessionCreate {
        name: String,
        password: String,
    },
    #[serde(rename = "authenticate")]
    Authenticate {
        token: SessionToken,
    },
    #[serde(rename = "room.create")]
    RoomCreate {
        name: String,
    },
    #[serde(rename = "room.get")]
    RoomGet {
        #[serde(rename = "roomId")]
        room_id: RoomId,
    },
    #[serde(rename = "room.join")]
    RoomJoin {
        #[serde(rename = "roomId")]
        room_id: RoomId,
    },
    #[serde(rename = "room.channel.get")]
    RoomChannelGet {
        #[serde(rename = "roomId")]
        room_id: RoomId,
    },
    #[serde(rename = "room.channel.create")]
    RoomChannelCreate {
        #[serde(rename = "roomId")]
        room_id: RoomId,
        name: String,
    },
    #[serde(rename = "channel.get")]
    ChannelGet {
        #[serde(rename = "channelId")]
        channel_id: ChannelId,
    },
    #[serde(rename = "channel.message.get")]
    ChannelMessageGet {
        #[serde(rename = "channelId")]
        channel_id: ChannelId,
    },
    #[serde(rename = "channel.message.create")]
    ChannelMessageCreate {
        #[serde(rename = "channelId")]
        channel_id: ChannelId,
        content: String,
    },
    #[serde(rename = "message.get")]
    MessageGet {
        #[serde(rename = "messageId")]
        message_id: MessageId,
    },
}

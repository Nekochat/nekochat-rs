use serde::{Serialize, Deserialize};

use super::{Message, Channel};

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "event")]
pub enum Event {
    #[serde(rename = "channelCreated")]
    ChannelCreated {
        channel: Channel,
    },
    #[serde(rename = "messageCreated")]
    MessageCreated {
        message: Message,
    },
}

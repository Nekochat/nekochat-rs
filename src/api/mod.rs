use chrono::{Utc, DateTime};
use serde::{Deserialize, Serialize, Serializer, Deserializer};
use uuid::Uuid;

pub mod request;
pub mod response;
pub mod event;

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub id: UserId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Room {
    pub id: RoomId,
    #[serde(rename = "ownerId")]
    pub owner_id: UserId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Channel {
    pub id: ChannelId,
    #[serde(rename = "roomId")]
    pub room_id: RoomId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Message {
    pub id: MessageId,
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "channelId")]
    pub channel_id: ChannelId,
    #[serde(rename = "createdTime")]
    #[serde(with = "serde_timestamp")]
    pub created_time: DateTime<Utc>,
    pub content: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct UserId(pub Uuid);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct RoomId(pub Uuid);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ChannelId(pub Uuid);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct MessageId(pub Uuid);

#[derive(Debug, Clone)]
pub struct SessionToken(pub Vec<u8>);

impl SessionToken {
    pub fn to_base64(&self) -> String {
        base64::encode(&self.0)
    }

    pub fn from_base64(base64: &str) -> Result<Self, base64::DecodeError> {
        Ok(Self(base64::decode(base64)?))
    }
}

impl Serialize for SessionToken {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        serializer.serialize_str(&self.to_base64())
    }
}
impl<'de> Deserialize<'de> for SessionToken {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: Deserializer<'de>
    {
        Self::from_base64(&String::deserialize(deserializer)?).map_err(serde::de::Error::custom)
    }
}

mod serde_timestamp {
    use chrono::{DateTime, Utc, TimeZone};
    use serde::{Deserializer, Serializer, Deserialize};

    pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        serializer.serialize_str(&date.timestamp_millis().to_string())
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
        where D: Deserializer<'de>,
    {
        Ok(Utc.timestamp_millis(String::deserialize(deserializer)?.parse().map_err(serde::de::Error::custom)?))
    }
}

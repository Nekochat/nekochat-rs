use serde::{Serialize, Deserialize};

use super::{Message, Room, Channel, User, SessionToken};

#[derive(Debug, Serialize)]
pub struct Response {
    pub id: u32,
    #[serde(with = "serde_status")]
    #[serde(flatten)]
    pub body: Status,
}

pub type Status = Result<Body, Error>;

#[derive(Debug)]
pub enum Error {
    InvalidCredentials,
    Unauthenticated,
    Unauthorized,
    NotFound,
    NameTaken,
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum Body {
    #[serde(rename = "user.create")]
    UserCreate(UserCreate),
    #[serde(rename = "user.get")]
    UserGet(UserGet),
    #[serde(rename = "user.get.self")]
    UserGetSelf(UserGetSelf),
    #[serde(rename = "user.get.self.room")]
    UserGetSelfRoom(UserGetSelfRoom),
    #[serde(rename = "session.create")]
    SessionCreate(SessionCreate),
    #[serde(rename = "auth.login")]
    AuthLogin(AuthLogin),
    #[serde(rename = "room.create")]
    RoomCreate(RoomCreate),
    #[serde(rename = "room.get")]
    RoomGet(RoomGet),
    #[serde(rename = "room.join")]
    RoomJoin(RoomJoin),
    #[serde(rename = "room.channel.get")]
    RoomChannelGet(RoomChannelGet),
    #[serde(rename = "room.channel.create")]
    RoomChannelCreate(RoomChannelCreate),
    #[serde(rename = "channel.get")]
    ChannelGet(ChannelGet),
    #[serde(rename = "channel.message.get")]
    ChannelMessageGet(ChannelMessageGet),
    #[serde(rename = "channel.message.create")]
    ChannelMessageCreate(ChannelMessageCreate),
    #[serde(rename = "message.get")]
    MessageGet(MessageGet),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserCreate {}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserGet {
    pub user: User,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserGetSelf {
    pub user: User,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserGetSelfRoom {
    pub rooms: Vec<Room>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SessionCreate {
    pub token: SessionToken,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthLogin {}

#[derive(Debug, Serialize, Deserialize)]
pub struct RoomCreate {
    pub room: Room,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RoomGet {
    pub room: Room,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RoomJoin;

#[derive(Debug, Serialize, Deserialize)]
pub struct RoomChannelGet {
    pub channels: Vec<Channel>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RoomChannelCreate {
    pub channel: Channel,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChannelGet {
    pub channel: Channel,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChannelMessageGet {
    pub messages: Vec<Message>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChannelMessageCreate {
    pub message: Message,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MessageGet {
    pub message: Message,
}

mod serde_status {
    use serde::{Serialize, Serializer};

    use super::{Body, Error, Status};

    pub fn serialize<'a, S>(status: &Status, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        #[derive(Serialize)]
        #[serde(tag = "status")]
        enum StatusFlat<'a> {
            #[serde(rename = "ok")]
            Ok(&'a Body),
            #[serde(rename = "invalidCredentials")]
            InvalidCredentials,
            #[serde(rename = "unauthenticated")]
            Unauthenticated,
            #[serde(rename = "unauthorized")]
            Unauthorized,
            #[serde(rename = "notFound")]
            NotFound,
            #[serde(rename = "nameTaken")]
            NameTaken,
        }

        match status {
            Ok(body) => StatusFlat::Ok(body),
            Err(error) => match error {
                Error::InvalidCredentials => StatusFlat::InvalidCredentials,
                Error::Unauthenticated => StatusFlat::Unauthenticated,
                Error::Unauthorized => StatusFlat::Unauthorized,
                Error::NotFound => StatusFlat::NotFound,
                Error::NameTaken => StatusFlat::NameTaken,
            },
        }.serialize(serializer)
    }
}

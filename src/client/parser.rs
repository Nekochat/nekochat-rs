use std::{collections::VecDeque, mem};

use serde::Deserialize;

use crate::api::{event::Event, response::{UserCreate, UserGet, UserGetSelf, UserGetSelfRoom, SessionCreate, AuthLogin, RoomCreate, RoomGet, RoomJoin, RoomChannelGet, RoomChannelCreate, ChannelGet, ChannelMessageGet, ChannelMessageCreate, MessageGet}};

pub struct Parser {
    buffer: Vec<u8>,
    packet_queue: VecDeque<Packet>,
}

impl Parser {
    pub fn new() -> Self {
        Self {
            buffer: Vec::new(),
            packet_queue: VecDeque::new(),
        }
    }
    pub fn parse(&mut self, bytes: &[u8]) {
        for byte in bytes {
            match byte {
                0x0a => {
                    self.packet_queue.push_back(serde_json::from_str(&String::from_utf8(mem::replace(&mut self.buffer, Vec::new())).unwrap()).unwrap())
                },
                _ => self.buffer.push(*byte),
            }
        }
    }

    pub fn get_packet(&mut self) -> Option<Packet> {
        self.packet_queue.pop_front()
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum Packet {
    Response(ReceivedResponse),
    Event(Event),
}

#[derive(Debug, Deserialize)]
pub struct ReceivedResponse {
    pub id: u32,
    #[serde(flatten)]
    body: serde_json::Value,
}

impl ReceivedResponse {
    pub fn try_into_user_create(self) -> Result<UserCreate, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_user_get(self) -> Result<UserGet, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_user_get_self(self) -> Result<UserGetSelf, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_user_get_self_room(self) -> Result<UserGetSelfRoom, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_session_create(self) -> Result<SessionCreate, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_auth_login(self) -> Result<AuthLogin, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_room_create(self) -> Result<RoomCreate, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_room_get(self) -> Result<RoomGet, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_room_join(self) -> Result<RoomJoin, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_room_channel_get(self) -> Result<RoomChannelGet, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_room_channel_create(self) -> Result<RoomChannelCreate, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_channel_get(self) -> Result<ChannelGet, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_channel_message_get(self) -> Result<ChannelMessageGet, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_channel_message_create(self) -> Result<ChannelMessageCreate, serde_json::Error> {
        serde_json::from_value(self.body)
    }

    pub fn try_into_message_get(self) -> Result<MessageGet, serde_json::Error> {
        serde_json::from_value(self.body)
    }
}

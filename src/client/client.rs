use std::{collections::HashMap, sync::Arc};

use tokio::{net::TcpStream, io::{AsyncReadExt, AsyncWriteExt}, sync::{mpsc, oneshot, Mutex}};

use crate::api::{request::{Request, RequestKind}, event::Event, response::{Error, self}, SessionToken, UserId, User, RoomId, Room, Channel, ChannelId, MessageId, Message};
use super::parser::{Parser, Packet, ReceivedResponse};

pub struct Client {
    request_queue_send: mpsc::Sender<(oneshot::Sender<ReceivedResponse>, RequestKind)>,
    event_queue_receive: Option<mpsc::Receiver<Event>>,
    token: Option<SessionToken>,
}

impl Client {
    pub async fn new(addr: &str) -> Self {
        let (mut stream_read, mut stream_write) = TcpStream::connect(addr).await.unwrap().into_split();
        let (request_queue_send, mut request_queue_receive) = mpsc::channel(8);
        let (event_queue_send, event_queue_receive) = mpsc::channel(8);
        let requests = Arc::new(Mutex::new(HashMap::<u32, oneshot::Sender<_>>::new()));

        {
            let requests = requests.clone();
            tokio::spawn(async move {
                let mut parser = Parser::new();

                loop {
                    let mut bytes = [0; 128];
                    let byte_count = stream_read.read(&mut bytes).await.unwrap();

                    parser.parse(&bytes[0..byte_count]);

                    while let Some(packet) = parser.get_packet() {
                        match packet {
                            Packet::Response(response) => {
                                if let Some(response_sender) = requests.lock().await.remove(&response.id) {
                                    response_sender.send(response).unwrap();
                                }
                            },
                            Packet::Event(event) => event_queue_send.send(event).await.unwrap(),
                        }
                    }
                }
            });
        }

        tokio::spawn(async move {
            let mut id = 0;
            while let Some((response_send, kind)) = request_queue_receive.recv().await {
                let request = Request { id, kind };
                requests.lock().await.insert(id, response_send);
                stream_write.write_all(&[serde_json::to_vec(&request).unwrap().as_slice(), &[0x0a]].concat()).await.unwrap();

                id += 1;
            }
        });

        Self {
            request_queue_send,
            event_queue_receive: Some(event_queue_receive),
            token: None,
        }
    }

    pub fn event_receiver(&mut self) -> Option<mpsc::Receiver<Event>> {
        self.event_queue_receive.take()
    }

    pub async fn create_user(&self, name: String, password: String) -> Result<(), Error> {
        self.send_request(RequestKind::UserCreate { name, password }).await
            .try_into_session_create().unwrap();
        Ok(())
    }

    pub async fn create_session(&self, name: String, password: String) -> Result<SessionToken, Error> {
        let response::SessionCreate { token } = self.send_request(RequestKind::SessionCreate { name, password }).await
            .try_into_session_create().unwrap();
        Ok(token)
    }

    pub async fn login(&mut self, token: SessionToken) -> Result<(), Error> {
        self.send_request(RequestKind::Authenticate { token: token.clone() }).await
            .try_into_auth_login().unwrap();
        self.token = Some(token);
        Ok(())
    }

    pub async fn get_user(&mut self) -> Result<User, Error> {
        let response::UserGetSelf { user } = self.send_request(RequestKind::UserGetSelf).await
            .try_into_user_get_self().unwrap();
        Ok(user)
    }

    pub async fn get_rooms(&mut self) -> Result<Vec<Room>, Error> {
        let response::UserGetSelfRoom { rooms } = self.send_request(RequestKind::UserGetSelfRoom).await
            .try_into_user_get_self_room().unwrap();
        Ok(rooms)
    }

    pub async fn create_room(&mut self) -> Result<(), Error> {
        self.send_request(RequestKind::UserGetSelfRoom).await
            .try_into_room_create().unwrap();
        Ok(())
    }

    pub async fn send_request(&self, request_kind: RequestKind) -> ReceivedResponse {
        let (response_send, response_receive) = oneshot::channel();
        self.request_queue_send.send((response_send, request_kind)).await.unwrap();
        response_receive.await.unwrap()
    }
}

impl UserId {
    pub async fn get(&self, client: &Client) -> Result<User, Error> {
        let response::UserGet { user } = client.send_request(RequestKind::UserGet { user_id: self.clone() }).await
            .try_into_user_get().unwrap();
        Ok(user)
    }
}

impl RoomId {
    pub async fn get(&self, client: &Client) -> Result<Room, Error> {
        let response::RoomGet { room } = client.send_request(RequestKind::RoomGet { room_id: self.clone() }).await
            .try_into_room_get().unwrap();
        Ok(room)
    }

    pub async fn get_channels(&self, client: &Client) -> Result<Vec<Channel>, Error> {
        let response::RoomChannelGet { channels } = client.send_request(RequestKind::RoomChannelGet { room_id: self.clone() }).await
            .try_into_room_channel_get().unwrap();
        Ok(channels)
    }

    pub async fn join(&self, client: &Client) -> Result<(), Error> {
        client.send_request(RequestKind::RoomJoin { room_id: self.clone() }).await
            .try_into_room_join().unwrap();
        Ok(())
    }

    pub async fn create_channel(&self, client: &Client, name: String) -> Result<Channel, Error> {
        let response::RoomChannelCreate { channel } = client.send_request(RequestKind::RoomChannelCreate { room_id: self.clone(), name }).await
            .try_into_room_channel_create().unwrap();
        Ok(channel)
    }
}

impl ChannelId {
    pub async fn get(&self, client: &Client) -> Result<Channel, Error> {
        let response::ChannelGet { channel } = client.send_request(RequestKind::ChannelGet { channel_id: self.clone() }).await
            .try_into_channel_get().unwrap();
        Ok(channel)
    }

    pub async fn get_messages(&self, client: &Client) -> Result<Vec<Message>, Error> {
        let response::ChannelMessageGet { messages } = client.send_request(RequestKind::ChannelMessageGet { channel_id: self.clone() }).await
            .try_into_channel_message_get().unwrap();
        Ok(messages)
    }

    pub async fn send(&self, client: &Client, content: String) -> Result<Message, Error> {
        let response::ChannelMessageCreate { message } = client.send_request(RequestKind::ChannelMessageCreate { channel_id: self.clone(), content }).await
            .try_into_channel_message_create().unwrap();
        Ok(message)
    }
}

impl MessageId {
    pub async fn get(&self, client: &Client) -> Result<Message, Error> {
        let response::MessageGet { message } = client.send_request(RequestKind::MessageGet { message_id: self.clone() }).await
            .try_into_message_get().unwrap();
        Ok(message)
    }
}
